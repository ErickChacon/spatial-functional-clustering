---
title: "Brazil clusters simulation: Poisson linear model"
prerequisites:
    - data/processed/brazil-adm1-pop.rds
targets:
    - data/simulated/brazil-adm1-lm1.rds
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.height = 8, fig.width = 7)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

In this script, we simulate cluster of Brazil using the federative units and simulate the
number of cases per federative unit through the time.

## Load packages, read data, and source custom scripts

```{r}
rm(list = ls())

library(readr)
library(sf)
library(ggplot2)
library(dplyr)
library(igraph)
library(purrr)
library(tidyr)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_raw <- file.path(path_data, "raw")
path_processed <- file.path(path_data, "processed")
path_sim <- file.path(path_data, "simulated")

brazil <- readRDS(file.path(path_processed, "brazil-adm1-pop.rds"))
```

## Create clusters

First, we transform the Brazilian federal units to a graph with edges and vertices, to
later obtain the minimum spanning tree (MST).

```{r}
set.seed(43)

# create graph
adj_mat <- st_touches(brazil) %>% as("matrix")
adj_mat_weighted <- adj_mat * runif(length(adj_mat))
graph <- graph_from_adjacency_matrix(adj_mat_weighted, mode = "upper", weighted = TRUE)
plot(graph)

# obtain minimum spaning tree
mstgraph <- mst(graph)
plot(mstgraph)
```

We can create `n` clusters by removing `n-1` edges of the MST. In this case, we will
create `4` clusters.

```{r}
# remove the edges with higher weights
edgeid_rm <- order(E(mstgraph)$weight)[1:4]
graph_comp <- components(delete_edges(mstgraph, edgeid_rm))
membership <- graph_comp$membership
brazil$cluster <- factor(membership)

plot(delete_edges(mstgraph, edgeid_rm), vertex.color = membership)
```

Let's visualize the clusters we created based on Brazil federative units.

```{r, fig.height = 7}
ggplot(brazil) +
    geom_sf(aes(fill = cluster)) +
    theme_bw() +
    labs(title = "Brazil clusters", fill = "Cluster")
```

## Simulate fixed effects of clusters

```{r, fig.height = 7}
# parameters
K = length(unique(membership))
ntime = 100
time = seq(0, 1, length.out = ntime)

# compute fixed effects
X = poly(time, 2, raw = TRUE, simple = TRUE)
# beta = cbind(c(1, 0), c(-1, 0), c(0, 0), c(-3, 3), c(3, -3))
beta = cbind(c(1, 0), c(-1, 0), c(-3, 3), c(3, -3), c(1, 0))
beta0 = - colMeans(X %*% beta)
eta0 = rep(beta0, each = ntime) + X %*% beta
matplot(time, eta0, xlab = "time", ylab = expression(X * beta))
```

## Simulate linear predictor

Add noise with respect to each cluster.

```{r, fig.height = 7}
sigmas = c(0.01, 0.05, 0.02, 0.05, 0.015)

eta = purrr::map(
    1:nrow(brazil),
    ~ eta0[, brazil$cluster[.]] + rnorm(ntime, mean = 0, sd = sigmas[brazil$cluster[.]])
    )
eta = do.call(cbind, eta)
matplot(time, eta, type = "b", xlab = "time", ylab = expression(eta))
```

The risk is the exponential of the linear predictor.

```{r, fig.height = 7}
matplot(time, exp(eta), type = "b", xlab = "time", ylab = expression(exp(eta)))
```

## Simulate Poisson data

```{r, fig.height = 7}
lambda = exp(eta) * rep(brazil$population, each = ntime)
Y = matrix(rpois(nrow(brazil) * ntime, lambda = lambda), nrow = ntime)
matplot(time, Y, type = "l", xlab = "Time", ylab = "Number of cases")
```

Lets visualize the trends by clusters.

```{r, fig.height = 7, fig.width = 8}
ydf = setNames(as.data.frame(Y), 1:nrow(brazil)) |>
    mutate(time = time) |>
    pivot_longer(1:nrow(brazil), names_to = "region", names_transform = as.numeric) |>
        mutate(cluster = brazil$cluster[region])

ggplot(ydf) +
    geom_line(aes(x = time, y = value, group = region, color = factor(region)), linewidth = 0.3) +
    facet_wrap(~ cluster, scales = "free_y") +
    theme_bw() +
    theme(legend.position = "none", legend.text = element_text(size = 7)) +
    labs(title = "Number of cases by cluster per federal unit", color = "Cluster",
    y = "Number of cases", x = "Time")
```

## Save simulated data

```{r}
saveRDS(
    list(beta0 = beta0, beta = beta, sigma = sigmas, eta = eta, Y = Y, cluster = brazil$cluster),
    file.path(path_sim, "brazil-adm1-lm2.rds")
)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

