---
title: "Clusterize spatial domain"
prerequisites:
    - data/simulated/spatial-domain.rds
targets:
    - data/simulated/spatial-domain-clusters.rds
    - data/simulated/spatial-domain-graph.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.height = 8, fig.width = 7)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

In this script, we simulate clusters of regions using a spatial simulated domain.

## Load packages, read data, and source custom scripts

```{r}
rm(list = ls())

library(sf)
library(ggplot2)
library(igraph)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_raw <- file.path(path_data, "raw")
path_processed <- file.path(path_data, "processed")
path_sim <- file.path(path_data, "simulated")

geodata <- readRDS(file.path(path_sim, "spatial-domain.rds"))
```

## Create clusters

First, we transform the spatial domain to a graph with edges and vertices, to
later obtain the minimum spanning tree (MST).

```{r}
set.seed(81)

# create graph
adj_mat <- st_touches(geodata) %>% as("matrix")
adj_mat_weighted <- adj_mat * runif(length(adj_mat))
graph <- graph_from_adjacency_matrix(adj_mat_weighted, mode = "upper", weighted = TRUE)
plot(graph)

# obtain minimum spaning tree
mstgraph <- mst(graph)
plot(mstgraph)
```

We can create `n` clusters by removing `n-1` edges of the MST. In this case, we will
create `10` clusters.

```{r}
# remove the edges with higher weights
n = 10
edgeid_rm <- order(E(mstgraph)$weight)[1:(n-1)]
graph_comp <- components(delete_edges(mstgraph, edgeid_rm))
membership <- graph_comp$membership
geodata$cluster <- factor(membership)

plot(delete_edges(mstgraph, edgeid_rm), vertex.color = hcl.colors(n, palette = "Set 2")[membership])
```

Let's visualize the clusters we created.

```{r, fig.height = 7}
ggplot(geodata) +
    geom_sf(aes(fill = cluster)) +
    theme_bw() +
    labs(title = "Spatial clusters", fill = "Cluster")
```

## Save simulated data

```{r}
saveRDS(geodata, file.path(path_sim, "spatial-domain-clusters.rds"))
saveRDS(graph, file.path(path_sim, "spatial-domain-graph.rds"))
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

