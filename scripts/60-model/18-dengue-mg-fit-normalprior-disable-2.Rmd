---
title: "Dengue in Minas Gerais fitting 2: Poisson random walk iid"
prerequisites:
    - data/processed/geodengue-mg.rds
    - data/modelled/dengue-mg-cluster-normalprior-disable.rds
targets:
    - data/modelled/dengue-mg-cluster-normalprior-disable-2.rds
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.height = 8, fig.width = 7)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data, and source custom scripts

```{r}
rm(list = ls())

library(sf)
library(ggplot2)
library(dplyr)
library(igraph)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")
devtools::load_all("/home/rstudio/documents/repositories/spfc")

data <- readRDS(file.path(path_processed, "geodengue-mg.rds"))
prevmodel <- readRDS(file.path(path_modelled, "dengue-mg-cluster-normalprior-disable.rds"))

domain <- data$domain
Y <- select(data$dengue, matches("^cases")) |> as.matrix()
N <- select(data$dengue, matches("^pop")) |> as.matrix()
E <- t(t(N) * colMeans(Y/N))
```

## Create graph components

```{r, fig.height = 16, fig.width = 14}
set.seed(7)

# define weights based on distances
distances <- as.matrix(dist(t(Y/(E + 0.0001))))
colnames(distances) <- NULL
rownames(distances) <- NULL

# create graph
adj_mat <- st_touches(domain) %>% as("matrix")
adj_mat_weighted <- adj_mat * (distances + 1)
graph <- graph_from_adjacency_matrix(adj_mat_weighted, mode = "upper", weighted = TRUE)
plot(graph, vertex.size = 7, vertex.label.cex = 1, edge.width = 1)

# obtain initial clusters
old_niter <- length(prevmodel$mst)
mstgraph <- prevmodel$mst[[old_niter]]
plot(mstgraph, vertex.size = 7, vertex.label.cex = 1, edge.width = 1)

membership <- prevmodel$cluster[old_niter,]
table(membership)

graphdata = list(graph = graph, mst = mstgraph, cluster = membership)
```

## Spatial clustering for Poisson

```{r, message = NA}
path_res <- file.path(path_modelled, "dengue-mg-cluster-normalprior-disable-2.rds")
formula <-  Yk ~ f(idt, model = "rw1", hyper = list(prec = list(prior = "normal", param = c(4, 1))))  +
    f(id, model = "iid", hyper = list(prec = list(prior = "normal", param = c(4, 1))))
niter  <- 50000
nsave <- 1000

set.seed(7)
system.time(
    bsfc(Y, graphdata = graphdata, N = E,
        formula = formula, family = "poisson", hyperpar = list(c = 0.99999999999),
        niter = niter, burnin = 0, thin = 1, path_save = path_res, nsave = nsave,
        control.inla = list(control.vb = list(enable = FALSE))
    )
)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```
