# Bayesian spatial latent clustering

Bayesian clustering

## Todo

- [ ] Linear mixed model
- [ ] Generalized linear mixed model
- [ ] Linear additive model
- [ ] Generalized additive model

## Package

- [ ] update igraph function
- [ ] `E(graph)$weight` instead of `E(graph)$weights`



- [ ]  Clustering using polynomial terms: Binomial
    - [ ] Simulate data
    - [ ] Vignette to fit model
- Clustering using random walk: Normal
    - [ ] Simulate data
- [ ] Covid 19: Poisson
    - [ ] Poisson

## Data

- vig1: stgauss (simple case)
- vig2: stbinom (show additional features)
- vig3: usacovid (real application)
