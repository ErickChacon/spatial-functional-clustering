target_all = docs/content/_index.html \
	docs/content/10-extract/_index.html \
	data/raw/brazil-adm1.rds \
	docs/content/10-extract/12-brazil-adm1.html \
	data/processed/geodengue-sp.rds \
	docs/content/10-extract/24-dengue-cases-sao_paulo.html \
	docs/content/30-process/_index.html \
	data/processed/brazil-adm1-pop.rds \
	docs/content/30-process/12-brazil-adm1-pop.html \
	docs/content/30-process/22-dengue-sp-standardise.html \
	docs/content/40-explore/_index.html \
	docs/content/40-explore/12-dengue-mg.html \
	docs/content/50-simulate/_index.html \
	data/simulated/spatial-domain.rds \
	docs/content/50-simulate/12-domain-create.html \
	data/simulated/spatial-domain-clusters.rds \
	data/simulated/spatial-domain-graph.rds \
	docs/content/50-simulate/14-domain-clusterize.html \
	docs/content/50-simulate/20-polinomial-non-spatial.html \
	data/simulated/stdata-polinomial.rds \
	data/simulated/sfclust-stbinom.rda \
	docs/content/50-simulate/22-polinomial-sim.html \
	data/simulated/stdata-splines.rds \
	data/simulated/sfclust-stgauss.rda \
	docs/content/50-simulate/32-splines-sim.html \
	docs/content/60-model/_index.html \
	docs/content/60-model/20-dengue-minas-gerais/_index.html \
	docs/content/60-model/30-dengue-sao-paulo/_index.html

target_clean = docs/content/_index.html \
	docs/content/10-extract/_index.html \
	docs/content/10-extract/12-brazil-adm1.html \
	docs/content/10-extract/24-dengue-cases-sao_paulo.html \
	docs/content/30-process/_index.html \
	docs/content/30-process/12-brazil-adm1-pop.html \
	docs/content/30-process/22-dengue-sp-standardise.html \
	docs/content/40-explore/_index.html \
	docs/content/40-explore/12-dengue-mg.html \
	docs/content/50-simulate/_index.html \
	docs/content/50-simulate/12-domain-create.html \
	docs/content/50-simulate/14-domain-clusterize.html \
	docs/content/50-simulate/20-polinomial-non-spatial.html \
	docs/content/50-simulate/22-polinomial-sim.html \
	docs/content/50-simulate/32-splines-sim.html \
	docs/content/60-model/_index.html \
	docs/content/60-model/20-dengue-minas-gerais/_index.html \
	docs/content/60-model/30-dengue-sao-paulo/_index.html

all: $(target_all)

docs/content/_index.html: \
	scripts/_index.Rmd

docs/content/10-extract/_index.html: \
	scripts/10-extract/_index.Rmd

data/raw/brazil-adm1.rds docs/content/10-extract/12-brazil-adm1.html: \
	scripts/10-extract/12-brazil-adm1.Rmd

data/processed/geodengue-sp.rds docs/content/10-extract/24-dengue-cases-sao_paulo.html: \
	scripts/10-extract/24-dengue-cases-sao_paulo.Rmd

docs/content/30-process/_index.html: \
	scripts/30-process/_index.Rmd

data/processed/brazil-adm1-pop.rds docs/content/30-process/12-brazil-adm1-pop.html: \
	scripts/30-process/12-brazil-adm1-pop.Rmd \
	data/raw/brazil-adm1.rds \
	data/raw/brazil-pop.csv

docs/content/30-process/22-dengue-sp-standardise.html: \
	scripts/30-process/22-dengue-sp-standardise.Rmd \
	data/processed/geodengue-sp.rds

docs/content/40-explore/_index.html: \
	scripts/40-explore/_index.Rmd

docs/content/40-explore/12-dengue-mg.html: \
	scripts/40-explore/12-dengue-mg.Rmd \
	data/processed/geodengue-mg.rds

docs/content/50-simulate/_index.html: \
	scripts/50-simulate/_index.Rmd

data/simulated/spatial-domain.rds docs/content/50-simulate/12-domain-create.html: \
	scripts/50-simulate/12-domain-create.Rmd

data/simulated/spatial-domain-clusters.rds data/simulated/spatial-domain-graph.rds docs/content/50-simulate/14-domain-clusterize.html: \
	scripts/50-simulate/14-domain-clusterize.Rmd \
	data/simulated/spatial-domain.rds

docs/content/50-simulate/20-polinomial-non-spatial.html: \
	scripts/50-simulate/20-polinomial-non-spatial.Rmd

data/simulated/stdata-polinomial.rds data/simulated/sfclust-stbinom.rda docs/content/50-simulate/22-polinomial-sim.html: \
	scripts/50-simulate/22-polinomial-sim.Rmd \
	data/simulated/spatial-domain-clusters.rds

data/simulated/stdata-splines.rds data/simulated/sfclust-stgauss.rda docs/content/50-simulate/32-splines-sim.html: \
	scripts/50-simulate/32-splines-sim.Rmd \
	data/simulated/spatial-domain-clusters.rds

docs/content/60-model/_index.html: \
	scripts/60-model/_index.Rmd

docs/content/60-model/20-dengue-minas-gerais/_index.html: \
	scripts/60-model/20-dengue-minas-gerais/_index.Rmd

docs/content/60-model/30-dengue-sao-paulo/_index.html: \
	scripts/60-model/30-dengue-sao-paulo/_index.Rmd

$(target_all):
	@Rscript -e 'blogdown:::build_rmds("$(<D)/$(<F)", "docs", "scripts")'

clean:
	rm -f $(target_clean)

